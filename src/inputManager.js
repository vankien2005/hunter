export class InputManager extends PIXI.Container {
  static get instance() {
    if (!this._instance) {
      this._instance = new InputManager();
    }
    return this._instance;
  }

  constructor() {
    super();
    this.left = keyboard(37);
    this.up = keyboard(38);
    this.right = keyboard(39);
    this.down = keyboard(40);
    this.left.press = () => this.emit("onleft");
    this.right.press = () => this.emit("onright");
    this.up.press = () => this.emit("onup");
    this.down.press = () => this.emit("ondown");
    this.down.release = () => this.emit("ondownrelease");
    this.right.release = () =>
      this.emit("onleftrightrelease", this.left.isDown);
    this.left.release = () =>
      this.emit("onleftrightrelease", this.right.isDown);
  }
}

function keyboard(keyCode) {
  var key = {};
  key.code = keyCode;
  key.isDown = false;
  key.isUp = true;
  key.press = undefined;
  key.release = undefined;
  key.downHandler = function (event) {
    if (event.keyCode === key.code) {
      if (key.isUp && key.press) key.press();
      key.isDown = true;
      key.isUp = false;
    }
    event.preventDefault();
  };

  key.upHandler = function (event) {
    if (event.keyCode === key.code) {
      if (key.isDown && key.release) key.release();
      key.isDown = false;
      key.isUp = true;
    }
    event.preventDefault();
  };

  window.addEventListener("keydown", key.downHandler.bind(key), false);
  window.addEventListener("keyup", key.upHandler.bind(key), false);
  return key;
}
